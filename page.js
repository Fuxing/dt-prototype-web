$(function () {
    var client = new WindowsAzure.MobileServiceClient('https://object.azure-mobile.net/', 'cvnQzPqZEvwdjPOdpCQYlXoSNbSMMz99'),
        todoItemTable = client.getTable('todoitem');

    // Read current data and rebuild UI.
    // If you plan to generate complex UIs like this, consider using a JavaScript templating library.
    function refreshTodoItems() {
        var query = todoItemTable.where({ complete: false });

        query.read().then(function (todoItems) {
            $('#todo-items').empty();
            $('#todo-items-optional').empty();
            $('#todo-items-physical').empty();
            $('#todo-items-table').empty();
            $('#todo-items-chair').empty();
            var listItems = $.map(todoItems, function (item) {
                if (item.type == 'physical') {
                    $('#todo-items-physical').append($('<li>')
                    .attr('data-todoitem-id', item.id).attr('draggable', 'true').attr('ondragstart', "dragStart(event,'" + item.id + "')")

                    .append($('<div>').append($('<input class="item-text">').val(item.text))
                    .append($('<button class="item-throw">Drag</button>'))
                    ));
                } else if (item.type == 'optional') {
                    $('#todo-items-optional').append($('<li>')
                    .attr('data-todoitem-id', item.id).attr('draggable', 'true').attr('ondragstart', "dragStart(event,'" + item.id + "')")

                    .append($('<div>').append($('<input class="item-text">').val(item.text))
                    .append($('<button class="item-throw">Drag</button>'))
                    ));

                } else if (item.type == 'table') {
                    $('#todo-items-table').append($('<li>')
                    .attr('data-todoitem-id', item.id).attr('draggable', 'true').attr('ondragstart', "dragStart(event,'" + item.id + "')")

                    .append($('<div>').append($('<input class="item-text">').val(item.text))
                    .append($('<button class="item-throw">Drag</button>'))
                    ));

                } else if (item.type == 'chair') {
                    $('#todo-items-chair').append($('<li>')
                    .attr('data-todoitem-id', item.id).attr('draggable', 'true').attr('ondragstart', "dragStart(event,'" + item.id + "')")

                    .append($('<div>').append($('<input class="item-text">').val(item.text))
                    .append($('<button class="item-throw">Drag</button>'))
                    ));

                } else {
                    $('#todo-items').append($('<li>')
                    .attr('data-todoitem-id', item.id).attr('draggable', 'true').attr('ondragstart', "dragStart(event,'" + item.id + "')")

                    .append($('<div>').append($('<input class="item-text">').val(item.text))
                     .append($('<button class="item-delete">Delete</button>'))
                    .append($('<button class="item-throw">Drag</button>'))
                    ));
                }

                return $('<li>')
                    .attr('data-todoitem-id', item.id).attr('draggable', 'true').attr('ondragstart', 'dragStart(event)')

                    .append($('<div>').append($('<input class="item-text">').val(item.text))
                     .append($('<button class="item-delete">Del</button>'))
                    .append($('<button class="item-throw">Drag</button>'))
                    );
            });

            //$('#todo-items').empty().append(listItems).toggle(listItems.length > 0);
            $('#summary').html('<strong>' + todoItems.length + '</strong> item(s)');
            setTimeout(function () {
                refreshTodoItems();
            }, 200);
        }, handleError);
    }

    function handleError(error) {
        var text = error + (error.request ? ' - ' + error.request.status : '');
        $('#errorlog').append($('<li>').text(text));
    }

    function getTodoItemId(formElement) {
        return $(formElement).closest('li').attr('data-todoitem-id');
    }

    // Handle insert
    $('#add-item').submit(function (evt) {
        var textbox = $('#new-item-text'),
            itemText = textbox.val();
        if (itemText !== '') {
            todoItemTable.insert({ text: itemText, complete: false }).then(refreshTodoItems, handleError);
        }
        textbox.val('').focus();
        evt.preventDefault();
    });

    // Handle update
    $(document.body).on('change', '.item-text', function () {
        var newText = $(this).val();
        todoItemTable.update({ id: getTodoItemId(this), text: newText }).then(null, handleError);
    });

    $(document.body).on('change', '.item-complete', function () {
        var isComplete = $(this).prop('checked');
        todoItemTable.update({ id: getTodoItemId(this), complete: isComplete }).then(refreshTodoItems, handleError);
    });

    // Handle delete
    $(document.body).on('click', '.item-delete', function () {
        todoItemTable.del({ id: getTodoItemId(this) }).then(refreshTodoItems, handleError);
    });

    // On initial load, start by fetching the current data
    refreshTodoItems();
});